proc mem.Alloc,size
    push    ebx
    push    ecx
    mov     ecx,[size]
    mcall   68,12
    pop     ecx
    pop     ebx
    ret
endp

proc mem.Free,mptr
    push    ebx
    push    ecx
    mov     ecx,[mptr]
    or      ecx,ecx
    jz      @f
@@: mcall   68,13
    pop     ecx
    pop     ebx
    ret
endp

proc mem.ReAlloc,mptr,size
    push    ebx
    push    ecx
    push    edx
    mov     ecx,[size]
    or      ecx,ecx
    jz      @f
@@: mov     edx,[mptr]
    or      edx,edx
    jz      @f
@@: mcall   68,20
    or      eax,eax
    jz      @f
@@: pop     edx
    pop     ecx
    pop     ebx
    ret
endp

