;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;                                     ;
;   RPL boot server                   ;
;                                     ;
;   License GPL                       ;
;                                     ;
;   Inspired by HTTPS.                ;
;                                     ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

appname     equ 'Kolibri RPL Server '
version     equ '0.1'

include     'macros.inc'
include     'proc32.inc'
include     'struct.inc'
include     '../../kernel/branches/net/applications/network.inc'
include     'rpl.inc'

header '01',1,start,i_end,mem,mem,,

REQ_DLL_VER = 2
DLL_ENTRY = 1
;============ CODE SECTION ================
start:          ;Load and initialize console.obj DLL

;------ LOAD DLL -------------
    stdcall     dll.Load,@IMPORT
    or          eax,eax
    jnz         .exit

;------- Check library version ---------
    cmp     word [con.version], REQ_DLL_VER ;DLL (max, min) version was imported by 'version' name
    jb      .exit
    cmp     word [con.version+2], REQ_DLL_VER
    ja      .exit
    push    DLL_ENTRY
    call    [con.start]

    mcall   40, 1 shl 7         ; we only want network events

;------- From now, we can use console ----
    push    caption
    push    -1
    push    -1
    push    -1
    push    -1
    call    [con.init]

;------- Print greeting ------------------
    push    hello_message
    call    [con.write_asciiz]

;------- Read config file ----------------
    call    read_ini_file

;------- Serve and obey ------------------
;------- Create LLC socket ---------------
    mcall   75, 0, AF_LLC, SOCK_DGRAM
    cmp     eax, -1
    jz      .exit
    mov     [sock_num], eax

;------- Bind to sockaddr_llc address struct
    mcall   bind, [sock_num], sockaddr1, sockaddr1.length
    cmp     eax, -1
    je      .bind_err


;   TODO:
;       a) bind to wildcard source MAC and 03:00:02:00:00:00 destination MAC
;       b) while server not stopped:
;             receive a LLC+RPL packet
;             get source MAC address
;             if it is FIND packet, check if MAC corresponds to some section in rpls.ini. If yes, send FOUND RPL packet
;             if it is SENDFILE packet, send configured file in RPL_DATARESPONSE

.loop:
    mcall   10

    mcall   recv, [sock_num], buffer, buffer.length
    cmp     eax, -1
    je      .loop

    mov     ax, [buffer + rpl_header.command]
    cmp     ax, RPL_CMD_FIND
    je      .rpl_send_found

    cmp
.rpl_cmd_find:
    ; TODO: check if we have boot image for source MAC. If we do, send RPL_FOUND packet
    jmp     .loop
.rpl_send_found:                ; send RPL_FOUND reply
    mov     cx, [RPL_FOUND.length]
    mov     esi, RPL_FOUND
    mov     edi, buffer
rep movsw
    mcall   send, [sock_num], buffer, RPL_FOUND.length
    jmp     .loop

.rpl_cmd_sendfile:
   ; TODO implement file transmission
   jmp      .loop

.bind_err:
    push    bind_error_msg
    call    [con.write_asciiz]
    jmp     .exit

;------- Close socket and console, then exit ----------
    mov        ecx, [sock_num]
    mcall      75, 1
    push    0
    call    [con.exit]
.exit:
    or      eax, -1
    int     0x40

proc read_ini_file
    invoke  ini.enum_sections,ini_name,print_ini_section
    ret
endp

proc print_ini_section,_filename,_section_name
    push    ebx                                 ;store EBX
    push    ecx


    mov     ebx, dword [host_section_prefix]   ;check that section name starts with "host"
    mov     ecx, dword [_section_name]
    cmp     ebx, dword [ecx]
    jnz     @f
    push    host_info_msg
    call    [con.write_asciiz]
    push    [_section_name]
    call    [con.write_asciiz]
    push    host_info_msg2
    call    [con.write_asciiz]
@@:
    pop     ecx
    pop     ebx
    or     eax, -1
    ret
endp

include 'mem.inc'
include 'dll.inc'

;============ DATA SECTION ================
BUFFERSIZE equ 1500

ini_name    db  '/hd0/1/rpls/rpls.ini',0
dll_name    db  '/sys/lib/console.obj',0
caption     db  'RPL boot server',0
hello_message   db  'Starting RPL server',10,0
host_section_prefix db 'host'
host_info_msg   db  'Configured host "',0
host_info_msg2  db  '"',10,0
bind_error_msg  db  'Cannot bind socket', 10, 0

sock_num        rd  1

sockaddr1:
          dw      AF_LLC
          rb      5       ; Unused fields
.sllc_sap rb      1       ; FIXME: use proper sap for RPL
.sllc_mac rb      6       ; MAC address
          rb      2       ; padding
.length = $ - sockaddr1

buffer    rb      BUFFERSIZE
.length = BUFFERSIZE
section @IMPORT

library libini,'libini.obj',\
        console,'console.obj'

import  libini,\
        ini.get_str,'ini_get_str',\
        ini.set_str,'ini_set_str',\
        ini.enum_sections,'ini_enum_sections'

import  console,con.start,'START',con.version,'version',con.init,'con_init',con.write_asciiz,'con_write_asciiz',con.exit,'con_exit'

;========== STACK =========================
i_end:
align 4
            rb  2048 ;stack
mem:

