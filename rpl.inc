struct rpl_header
    length          dw  0053h
    command         dw  0001h
ends

RPL_CMD_FIND            equ 0001h
RPL_CMD_FOUND           equ 0002h
RPL_CMD_SENDFILE        equ 0003h
RPL_CMD_DATARESPONSE    equ 0004h

struct RPL_FIND
    length          dw  0053h
    command         dw  RPL_CMD_FIND
    corvec          dd  00084003h
    corval          dd  00000000h
    connect_info    dd  00100008h
    frame_size      dd  00064009h
    max_frame       rb  2
    connect_class   dd  0006400Ah
    class_I         dw  0001h
    addr_vec        dd  000A4006h
    mac_addr        rb  6
    logical_sap_vec dd  00054007h
    remote_sap      dw  0FCh
    search_vec      dd  00280004h
    loader_info     dd  0024C005h
    int15_conf      rb  8
    int11_ax        rb  2
    memory_size     rb  2
    version         rb  2
    rest_of_ec      dd  0
                    dw  0
    adapter_id      dw  5432h
    short_name      rb  10
ends

struct RPL_FOUND
    length          dw  003Ah
    command         dw  0002h
    corvec          dd  00084003h
    corval          dd  00000000h
    resp_correlator dd  0005400Bh
    resp_code       db  0
    set_addr_vec    dd  000A400Ch
    gr_addr_unsup   dd  0
                    dw  0
    loader_addr_vec dd  000A4006h
    node_mac_addr   rb  6
    connect_info    dd  00100008h
    frame_size      dd  00064009h
    max_frame       rb  2
    connect_class   dd  0006400Ah
    class_I         dw  0001h
    loader_sap_vec  dd  00054007h
    server_sap      rb  1
ends

struct RPL_SENDFILE
ends

struct RPL_DATARESPONSE
ends
